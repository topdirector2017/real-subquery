import {LogSponsorLinked} from "../types";
import { MoonbeamEvent } from '@subql/contract-processors/dist/moonbeam';
import { BigNumber } from "ethers";

// LogSponsorLinked(address sponsor, address affiliate);
// Setup types from ABI
type LogSponsorLinkedEventArgs = [string, string] & { sponsor: string; affiliate: string; };

export async function handleLogSponsorLinkedEvent(event: MoonbeamEvent<LogSponsorLinkedEventArgs>): Promise<void> {
    const logSponsorLinked = new LogSponsorLinked(event.transactionHash);

    logSponsorLinked.sponsor = event.args.sponsor;
    logSponsorLinked.affiliate = event.args.affiliate;

    await logSponsorLinked.save();
}
